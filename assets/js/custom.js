
/*
  Site Name: E-Office Portal - Online office management system
  URI: http:http://eoffice.techware.co.in/
  Description: Customized JS for E-Office Portal
  Version: 2.0
  Author: Amal-Techware Solution
  Author URI: 
  Tags:
  
  ---------------------------
  CUSTOM CONPONENT-SCRIPTS
  ---------------------------
  
  TABLE OF CONTENTS
  ---------------------------
   01. FONTS-N-SIZES 
   02. CURRENT-TIME
   03. DATE-PICKER



*/

$(function(){


/*-------------------------------

01. HEADER-SHRINKER

--------------------------------*/

    $(window).scroll(function() {
  if ($(document).scrollTop() > 52) {
    $('.e_main_navbar').addClass('e_main_navbar_shrink');
  } else{
    $('.e_main_navbar').removeClass('e_main_navbar_shrink');
  }
});


/*-------------------------------

02. CURRENT-TIME

--------------------------------*/

var clockID;
var yourTimeZoneFrom = -7.00; //time zone value where you are at

var d = new Date();  
//get the timezone offset from local time in minutes
var tzDifference = yourTimeZoneFrom * 60 + d.getTimezoneOffset();
//convert the offset to milliseconds, add to targetTime, and make a new Date
var offset = tzDifference * 60 * 1000;

function UpdateClock() {
    var tDate = new Date(new Date().getTime()+offset);
    var in_hours = tDate.getHours()
    var in_minutes=tDate.getMinutes();
    var in_seconds= tDate.getSeconds();

    if(in_minutes < 10)
        in_minutes = '0'+in_minutes;
    if(in_seconds<10)   
        in_seconds = '0'+in_seconds;
    if(in_hours<10) 
        in_hours = '0'+in_hours;

   document.getElementById('theTime').innerHTML = "" 
                   + in_hours + "&nbsp;:&nbsp;" 
                   + in_minutes + "&nbsp;:&nbsp;" 
                   + in_seconds;

}
function StartClock() {
   clockID = setInterval(UpdateClock, 500);
}

function KillClock() {
  clearTimeout(clockID);
}
window.onload=function() {
  StartClock();
}

/*-------------------------------

03. DATE-PICKER

--------------------------------*/


$('#e_date_picker').datepicker({
    autoclose: true
});

$('#e_date_picker').on('show', function(e){
    console.debug('show', e.date, $(this).data('stickyDate'));
    
    if ( e.date ) {
         $(this).data('stickyDate', e.date);
    }
    else {
         $(this).data('stickyDate', null);
    }
});

$('#e_date_picker').on('hide', function(e){
    console.debug('hide', e.date, $(this).data('stickyDate'));
    var stickyDate = $(this).data('stickyDate');
    
    if ( !e.date && stickyDate ) {
        console.debug('restore stickyDate', stickyDate);
        $(this).datepicker('setDate', stickyDate);
        $(this).data('stickyDate', null);
    }
});


});

