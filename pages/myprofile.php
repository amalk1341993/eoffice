<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull heightFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				My Profile
			</div>

	<!-- PROFILE-DETAIL-DIV-STARTS -->


			<div class="e_content_wrapper">
				<div class="e_profile_detail_div">
						<div class="e_profile_detail_pic">
							<div class="e_profile_detail_pic_overlay"></div>
							<img src="../assets/img/e_pic4.jpg">
						</div>
						<div class="e_profile_detail">
							<h3>Ann Alexander</h3>
							<h5>Software Developer</h5>
							<p>EMP ID: TWS 134</p>
							<div class="e_profile_detail1">
								<li>
									<p class="e_msg">annalex@gmail.co.in</p>
									<p class="e_call">+91 9961531199</p>
								</li>
								<li class="borderNone" style="padding-left: 20px;">
									<p class="e_loc">Loremipsum , Dummy Street<br>Los Angles, 406 A</p>
								</li>
								<div class="clear"></div>
							</div>
						</div>
						<div class="e_experience">
							<img src="../assets/img/e_experience.png">
							<h5>Techware Experience</h5>
							<p><strong>1</strong>Year, <strong>11</strong> Months, <strong>8</strong>Days</p>
						</div>
						<div class="clear"></div>
				</div>
			</div>

			<hr>

	<!-- ENDS-PROFILE-DETAIL-DIV-STARTS -->

	<!-- APPLY-LEAVE-CONTENT -->

						<div class="e_apply_leave_content pl0 pr0">
							<div class="row">
								<div class="col-md-2">
									<div class="e_apply_leave_text e_profile_leave_dept">
										Department<br>
										<strong>SDU</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text e_profile_leave_dept">
										Join Date<br>
										<strong>28 - 08 - 2016</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text e_profile_leave_dept">
										Qualification<br>
										<strong>Graduate</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text e_profile_leave_dept">
										Date of Birth<br>
										<strong>1993-06-17</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text e_profile_leave_dept">
										Blood Group<br>
										<strong>O+</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text e_profile_leave_dept borderNone">
										Gender<br>
										<strong>Male</strong>
									</div>
								</div>
							</div>
						</div>

	<!-- ENDS-APPLY-LEAVE-CONTENT -->

						<div class="e_profile_btn_bay">
							<a href="editprofile.php">
								<button class="e_profile_edit_button ">Edit Profile<span><img src="../assets/img/e_profile_edit.png"></span>
								</button>
							</a>
							<button class="e_profile_changepass_button" data-toggle="modal" data-target="#changepass">Change Password<span><img src="../assets/img/e_profile_pass.png"></span></button>
						</div>

		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
<?php
	include "../includes/footer.php";
?>




