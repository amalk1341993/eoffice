<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				My Attendence Status
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

					</div>
				</div>
			</div>
			<hr>

	<!-- ATTENDENCE-STATUS -->

			<div class="e_attendence_status">
				<div class="row">
					<div class="col-md-8">
						<p>Select month and year to generate report</p>
						<div class="row">
							<div class="col-md-4"><h6>Select Month</h6></div>
							<div class="col-md-4"><h6>Select Year</h6></div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<select class="e_attendence_select">
									<option>Month</option>
								</select>
							</div>
							<div class="col-md-4">
								<select class="e_attendence_select">
									<option>Year</option>
								</select>	
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-3 textRight">
								<button class="e_attendence_submit mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">Submit</button>
							</div>
						</div>
						<br>
						<p>Users Attendance Report Info</p>
						<div class="row">
							<div class="col-md-8">
								<div class="e_attendence_report_info">
									<div class="e_attendence_report_pic">
										<img src="../assets/img/e_pic3.jpg">
									</div>
									<div class="e_attendence_report_detail">
										<ul>
											<li>
												<div class="child1">Username</div>
												<div class="child2">:&nbsp;&nbsp;&nbsp;&nbsp;James Walton</div>
												<div class="clear"></div>
											</li>
											<li>
												<div class="child1">Employee ID</div>
												<div class="child2">:&nbsp;&nbsp;&nbsp;&nbsp;TWS 123</div>
												<div class="clear"></div>
											</li>
											<li>
												<div class="child1">Report month/Year</div>
												<div class="child2">:&nbsp;&nbsp;&nbsp;&nbsp;May 2017</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="clear"></div>
								</div>
							</div>
							<div class="col-md-4"></div>
						</div>
					</div>
					<div class="col-md-4"></div>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-md-12">

	<!-- LEAVE-STATUS-TABLES -->

						<div class="e_leave_status_table">

	<!-- LEAVE-STATUS-HEAD -->

							<div class="e_leave_status_head">
								<div class="row">
									<div class="col-md-1">
										<select class="e_leave_status_sl">
											<option>10</option>
											<option>20</option>
										</select>
									</div>
									<div class="col-md-2"><span>Sort by:</span>
										<select class="e_leave_status_sort">
											<option>Date</option>
											<option>Time</option>
										</select>
									</div>
									<div class="col-md-5"></div>
									<div class="col-md-4">
										<div class="e_leave_search_box">
											<input class="e_leave_search_input" placeholder="Search here">
										</div>
									</div>
								</div>
							</div>

	<!-- ENDS-LEAVE-STATUS-HEAD -->

	<!-- LEAVE-STATUS-TABLE-DIV -->

							<div class="table-responsive">          
							  	<table class="table">
							    	<thead>
							      		<tr>
									        <th>SL</th>
									        <th>Signin Date</th>
									        <th>Sigin Time</th>
									        <th>Sigout Time</th>
									        <th>Working Hours</th>
									        <th>Signin Note</th>
									        <th>Signout Note</th>
									        <th>Status</th>
							      		</tr>
							    	</thead>
								    <tbody>
								      <tr>
								        <td>01</td>
								        <td>05-May-2017</td>
								        <td>08:40 am</td>
								        <td>06:00 pm</td>
								        <td>09 hrs 30 mins</td>
								        <td>No Internet</td>
								        <td>Meeting</td>
								        <td><div class="e_completed">Complete<i class="fa fa-check" aria-hidden="true"></i></div></td>
								      </tr>
								      <tr>
								        <td>02</td>
								        <td>05-May-2017</td>
								        <td>08:40 am</td>
								        <td>06:00 pm</td>
								        <td>09 hrs 30 mins</td>
								        <td>No Internet</td>
								        <td>Meeting</td>
								        <td><div class="e_holiday">Holiday<i class="fa fa-circle-o" aria-hidden="true"></i></div></td>
								      </tr>
								       <tr>
								        <td>03</td>
								        <td>05-May-2017</td>
								        <td>08:40 am</td>
								        <td>06:00 pm</td>
								        <td>09 hrs 30 mins</td>
								        <td>No Internet</td>
								        <td>Meeting</td>
								        <td><div class="e_leave">Leave<i class="fa fa-info" aria-hidden="true"></i></div></td>
								      </tr>
								    </tbody>
							  	</table>
			 				</div>


	<!-- ENDS-LEAVE-STATUS-TABLE-DIV -->

						</div>

	<!-- APPLY-LEAVE-CONTENT -->

						<div class="e_apply_leave_content pl0 pr0">
							<div class="row">
								<div class="col-md-2">
									<div class="e_apply_leave_text">
										Total Working Hours<br>
										<strong class="color1">210:20:40</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text">
										Incomplete Days<br>
										<strong class="color3">0</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text">
										Late Puchin Days<br>
										<strong class="color3">0</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text">
										Completed Days<br>
										<strong class="color5">20</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text">
										Total Leave<br>
										<strong class="color4">1</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text borderNone">
										Total Holidays<br>
										<strong class="color2">1</strong>
									</div>
								</div>
							</div>
						</div>

	<!-- ENDS-APPLY-LEAVE-CONTENT -->

					</div>
				</div>
			</div>

	<!-- ENDS-ATTENDENCE-STATUS -->
	
	<!-- ENDS-PARTITION -->



		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


