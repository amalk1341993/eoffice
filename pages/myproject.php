<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull heightFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Leave Status
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

					</div>
				</div>
			</div>
			<hr>

	<!-- ENDS-PARTITION -->

	<!-- LEAVE-STATUS-TABLES -->

			<div class="row">
				<div class="col-md-8">
					<div class="e_leave_status_table">

	<!-- LEAVE-STATUS-HEAD -->
						<div class="e_content_head">
							<div class="e_title_circle color1 floatLeft">
								<img src="../assets/img/e_p7.png">
							</div>
							<h5 class="floatLeft">My Project List</h5>
							<div class="clear"></div>				
						</div>
						<br><br>

						<div class="e_leave_status_head">
							<div class="row">
								<div class="col-md-2">
									<select class="e_leave_status_sl">
										<option>10</option>
										<option>20</option>
									</select>
								</div>
								<div class="col-md-3"><span>Sort by:</span>
									<select class="e_leave_status_sort">
										<option>Date</option>
										<option>Time</option>
									</select>
								</div>
								<div class="col-md-3"></div>
								<div class="col-md-4">
									<div class="e_leave_search_box">
										<input class="e_leave_search_input" placeholder="Search here">
									</div>
								</div>
							</div>
						</div>

	<!-- ENDS-LEAVE-STATUS-HEAD -->

	<!-- LEAVE-STATUS-TABLE-DIV -->

						<div class="table-responsive">          
						  	<table class="table">
						    	<thead>
						      		<tr>
								        <th>SL</th>
								        <th>Project Name</th>
						      		</tr>
						    	</thead>
							    <tbody>
							      <tr>
							        <td>1</td>
							        <td>Book my Doc</td>
							      </tr>
							      <tr>
							        <td>2</td>
							        <td>Book my saloon</td>
							      </tr>
							      <tr>
							        <td>3</td>
							        <td>Omega Nutri</td>
							      </tr>
							    </tbody>
						  	</table>
		 				</div>


 	<!-- ENDS-LEAVE-STATUS-TABLE-DIV -->

					</div>

	<!-- ENDS-LEAVE-STATUS-TABLES -->
					
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


