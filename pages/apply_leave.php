<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull heightFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Apply Leave
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

	<!-- APPLY-LEAVE-HEAD -->

						<div class="e_content_head theme_grey">
							<div class="e_title_circle color1 floatLeft">
								<img src="../assets/img/e_p5.png">
							</div>
							<h5 class="floatLeft">Leave Management</h5>
							<div class="clear"></div>				
						</div>


	<!-- ENDS-APPLY-LEAVE-HEAD -->

	<!-- APPLY-LEAVE-CONTENT -->

						<div class="e_apply_leave_content">
							<div class="row">
								<div class="col-md-3">
									<div class="e_apply_leave_text">
										Leave Permission<br>
										<strong class="color1">2</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text">
										Carry Left<br>
										<strong class="color2">0</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text">
										Leave Taken<br>
										<strong class="color2">0</strong>
									</div>
								</div>
								<div class="col-md-2">
									<div class="e_apply_leave_text borderNone">
										Remaining Leave<br>
										<strong class="color3">1</strong>
									</div>
								</div>
								<div class="col-md-3">
									<button class="e_work_add_btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect floatRight" data-toggle="modal" data-target="#e_applyleave">Apply<i class="fa fa-check" aria-hidden="true"></i></button>

	<!-- APPLY-LEAVE-MODAL -->
									
										<div id="e_applyleave" class="modal fade" role="dialog">
											<div class="modal-dialog">
											    <div class="modal-content e_apply_leave_modal">
											     	<div class="e_apply_leave_container">
											   	 		<div class="e_content_head">
															<div class="e_title_circle color1 floatLeft">
																<img src="../assets/img/e_p6.png">
															</div>
															<h5 class="floatLeft">Leave apply details info</h5>
															<div class="clear"></div>				
														</div>
														<div class="e_apply_modal_description">
															<div class="row">
																<div class="col-md-6">
																	<p>Leave Date</p>
																	<input class="e_apply_modal_input" id="e_date_picker" type="text" placeholder="dd mm yyyy">
																</div>
																<div class="col-md-6 p0">
																	<p>Leave Type</p>
																	<div class="e_radio_button_div">
																		<label class="e_radio_button inline"> 
																			   <input type="radio" name="report" value="summary" checked>
																			   <span>Full Day</span> 
																		</label>
																		<label class="e_radio_button inline"> 
																			    <input type="radio" name="report" value="detailed">
																			    <span>Half Day</span> 
										  								</label>
																	</div>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-12">
																	<p>Leave Reason</p>
																	<textarea class="e_apply_modal_textarea" rows="4"></textarea>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<button class="e_apply_modal_submit">Submit</button>
																</div>
															</div>
														</div>
										   	 		</div>
											    </div>
											</div>
										</div>

	<!-- ENDS-APPLY-LEAVE-MODAL -->

									<div class="clear"></div>	
								</div>
							</div>
						</div>

	<!-- ENDS-APPLY-LEAVE-CONTENT -->

					</div>
				</div>
			</div>

	<!-- ENDS-PARTITION -->

	<!-- LEAVE-DETAILS -->

			<div class="e_leave_details">
				<div class="row">
					<div class="col-md-8">

	<!-- LEAVE-INFORMATION -->

						<div class="e_leave_information">

	<!-- LEAVE-INFORMATION-HEAD -->

							<div class="e_content_head theme_grey">
								<div class="e_title_circle color1 floatLeft">
									<img src="../assets/img/e_p6.png">
								</div>
								<h5 class="floatLeft">Leave Information</h5>
								<div class="clear"></div>				
							</div>

	<!-- ENDS-LEAVE-INFORMATION-HEAD -->

	<!-- OFFICE-HOLIDAY-LIST -->

							<div class="e_office_holiday_list">
								<div class="row">
									<div class="col-md-6">
										<div class="e_year_title">
											<p>2015</p>
											<h6>Techware Holiday List</h6>
										</div>										
									</div>
									<div class="col-md-6">
										<button class="e_holiday_info_btn">Holiday Info</button>
									</div>
								</div>
								<ul>
									<li>
										<div class="row">
											<div class="col-md-1">
												<h5>SI#</h5>
											</div>
											<div class="col-md-11">
												<h5>National Holidays - 2015</h5>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col-md-1">
												<p>1</p>
											</div>
											<div class="col-md-3">
												<p>26th January, 2015</p>
											</div>
											<div class="col-md-3">
												<p>Republic Day</p>
											</div>
											<div class="col-md-5">
												<p>Monday</p>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col-md-1">
												<p>2</p>
											</div>
											<div class="col-md-3">
												<p>26th January, 2015</p>
											</div>
											<div class="col-md-3">
												<p>Republic Day</p>
											</div>
											<div class="col-md-5">
												<p>Monday</p>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col-md-1">
												<p>3</p>
											</div>
											<div class="col-md-3">
												<p>26th January, 2015</p>
											</div>
											<div class="col-md-3">
												<p>Republic Day</p>
											</div>
											<div class="col-md-5">
												<p>Monday</p>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col-md-1">
												<p>4</p>
											</div>
											<div class="col-md-3">
												<p>26th January, 2015</p>
											</div>
											<div class="col-md-3">
												<p>Republic Day</p>
											</div>
											<div class="col-md-5">
												<p>Monday</p>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col-md-1">
												<p>5</p>
											</div>
											<div class="col-md-3">
												<p>26th January, 2015</p>
											</div>
											<div class="col-md-3">
												<p>Republic Day</p>
											</div>
											<div class="col-md-5">
												<p>Monday</p>
											</div>
										</div>
									</li>
								</ul>
							</div>

	<!-- ENDS-OFFICE-HOLIDAY-LIST -->
	
						</div>

	<!-- ENDS-LEAVE-INFORMATION -->

					</div>
					<div class="col-md-4">

	<!-- SPECIAL-NOTES -->

						<div class="e_special_notes">

	<!-- SPECIAL-NOTES-HEAD -->

							<div class="e_content_head">
								<div class="e_title_circle color1 floatLeft">
									<img src="../assets/img/e_p6.png">
								</div>
								<h5 class="floatLeft">Special Notes</h5>
								<div class="clear"></div>				
							</div>

	<!-- ENDS-SPECIAL-NOTES -->

	<!-- SPECIAL-NOTES-DETAILS -->

							<div class="e_special_notes_details">
								<ul>
									<li>
										December 24 - to be worked on 12th December (Before Christmas), who are needed.
										
									</li>
									<li>
										December 24 - to be worked on 12th December (Before Christmas), who are needed.
										
									</li>
								</ul>
							</div>

	<!-- ENDS-SPECIAL-NOTES-DETAILS -->

						</div>

	<!-- ENDS-SPECIAL-NOTES -->

					</div>
				</div>
			</div>

	<!-- ENDS-LEAVE-DETAILS -->

		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>









