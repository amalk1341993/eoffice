<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull heightFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Thought of the day
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

					</div>
				</div>
			</div>
			<hr>

	<!-- THOUGHT-OF-THE-DAY-DIV -->

			<div class="e_thought_div">

	<!-- THOUGHT-DIV-LEFT-->

				<div class="e_thought_div_left">
					<img src="../assets/img/e_thought.png">
				</div>

	<!-- ENDS-THOUGHT-DIV-LEFT -->

	<!-- THOUGHT-DIV-RIGHT-->

				<div class="e_thought_div_right">
					<p>“ One small positive thought in the morning <br>
   can change your whole day ”</p>
				</div>

	<!-- ENDS-THOUGHT-DIV-RIGHT -->

				<div class="clear"></div>

			</div>
	
	<!-- END-THOUGHT-OF-THE-DAY-DIV -->		

	<!-- ENDS-PARTITION -->



		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


