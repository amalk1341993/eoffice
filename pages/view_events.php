<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Events
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

					</div>
				</div>
			</div>
			<hr>

	<!-- THOUGHT-OF-THE-DAY-DIV -->

			<div class="e_thought_div">

	<!-- THOUGHT-DIV-LEFT-->

				<div class="e_thought_div_left">
					<img src="../assets/img/e_event1.png">
				</div>

	<!-- ENDS-THOUGHT-DIV-LEFT -->

	<!-- THOUGHT-DIV-RIGHT-->

				<div class="e_event_list_div_right">

	<!-- LEAVE-STATUS-TABLES -->

					<div class="e_leave_status_table">

	<!-- LEAVE-STATUS-HEAD -->

						<div class="e_leave_status_head">
							<div class="row">
								<div class="col-md-2">
									<select class="e_leave_status_sl">
										<option>10</option>
										<option>20</option>
									</select>
								</div>
								<div class="col-md-3"><span>Sort by:</span>
									<select class="e_leave_status_sort">
										<option>Date</option>
										<option>Time</option>
									</select>
								</div>
								<div class="col-md-3"></div>
								<div class="col-md-4">
									<div class="e_leave_search_box">
										<input class="e_leave_search_input" placeholder="Search here">
									</div>
								</div>
							</div>
						</div>

	<!-- ENDS-LEAVE-STATUS-HEAD -->

	<!-- LEAVE-STATUS-TABLE-DIV -->

						<div class="table-responsive">          
						  	<table class="table">
						    	<thead>
						      		<tr>
								        <th>Sl</th>
								        <th>Title</th>
								        <th style="width: 45%;">Description of the Event</th>
								        <th> Date</th>
								        <th>View</th>
						      		</tr>
						    	</thead>
							    <tbody>
							      <tr>
							        <td>1</td>
							        <td>Onam Celebration</td>
							        <td>
							        	Dear All,<br><br>

With Immense joy and pleasure, we would like to inform you that this year 'Techware Onam Celebrations' will be held on 27th August 2015 with variety of events along with ONAM SADHYA. <br><br> 

Venue: Techware Office<br>

Date: 27th August 2015<br>

Time: 9.00 AM to 3.00 PM<br><br>


<strong>Note:</strong> <br><br>


 1. Compulsory attendance will be maintained for the day.<br>

 2. Gents dress code: Mundu and Kurtha/Shirt.<br>

      Ladies dress code: Kerala Saree
							        </td>
							        <td>05-May-2017</td>
							        <td class="viewall"></td>
							      </tr>
							      <tr>
							        <td>2</td>
							        <td>Onam Celebration</td>
							        <td>
							        	Dear All,<br><br>

With Immense joy and pleasure, we would like to inform you that this year 'Techware Onam Celebrations' will be held on 27th August 2015 with variety of events along with ONAM SADHYA. <br><br> 

Venue: Techware Office<br>

Date: 27th August 2015<br>

Time: 9.00 AM to 3.00 PM<br><br>


<strong>Note:</strong> <br><br>


 1. Compulsory attendance will be maintained for the day.<br>

 2. Gents dress code: Mundu and Kurtha/Shirt.<br>

      Ladies dress code: Kerala Saree
							        </td>
							        <td>05-May-2017</td>
							        <td class="viewall"></td>
							      </tr>
							    </tbody>
						  	</table>
		 				</div>


 	<!-- ENDS-LEAVE-STATUS-TABLE-DIV -->

					</div>

	<!-- ENDS-LEAVE-STATUS-TABLES -->
		
				</div>

	<!-- ENDS-THOUGHT-DIV-RIGHT -->

				<div class="clear"></div>

			</div>
	
	<!-- END-THOUGHT-OF-THE-DAY-DIV -->		

	<!-- ENDS-PARTITION -->

		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


