<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Welcome <a>Sophia Lewis..</a>
			</div>

	<!-- PARTITION-STARTS -->

		<div class="row">

	<!-- LEFT-PART-FEEDS -->

			<div class="col-md-8">
				<div class="e_content_wrapper">

	<!-- FEEDS-HEADER -->

					<div class="e_content_head theme_grey">
						<div class="e_title_circle color1 floatLeft">
							<img src="../assets/img/e_feeds.png">
						</div>
						<h5 class="floatLeft">Techware Feeds</h5>
						<div class="clear"></div>				
					</div>

	<!-- END-FEEDS-HEADER -->

					<div class="e_content_feed_list">

	<!-- FEEDS-LIST -->

						<ul>
							<li>
								<div class="e_feed_title">
									<div class="e_title_circle color2 floatLeft">
										<img src="../assets/img/e_info.png">
									</div>
									<div class="e_feed_inner floatLeft">
										<h6>Announcement Title:</h6>
										<h4>Eoffice Attendance Marking - Mandatory</h4>
									</div>
									<div class="clear"></div>
								</div>
								<div class="e_feed_content">
									<h6>Announcement Info:</h6>
									<div class="e_feeds_image">
										<ul>
											<li><img src="../assets/img/e_office1.png"></li>
											<li><img src="../assets/img/e_office2.png"></li>
											<div class="clear"></div>
										</ul>
									</div>
									<div class="e_feeds_messages">
										<p>Hi Team, </p>
										<p>It has been observed most employees were using “Custom Attendance”option in Eoffice for marking attendance instead of “Daily Attendance”. </p>
										<p>Custom Attendance markings can be used only when there are any Eoffice or network problems you are facing at the time of login. It must be done with proper explanation of reason with the approval of your respective Team Lead or appropriate person. </p>
										<p>Kindly take a note of the same.Looking forward for a positive outcome.</p>
										<p>Regards<br>
										HR</p>
									</div>
									<div class="e_feed_btm">
										<div class="e_feed_view floatLeft">View</div>
										<div class="e_feed_daten_time floatLeft">
											<span><img src="../assets/img/e_timeout.png"></span>
											<span>20&nbsp;/&nbsp;05&nbsp;/&nbsp;2017</span>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<span>10&nbsp;:&nbsp;00&nbsp;am</span>
										</div>
										<div class="clear"></div>
									</div>
									<br>	
									<hr>
									<br>
								</div>
							</li>
							<li>
								<div class="e_feed_title">
									<div class="e_title_circle color3 floatLeft">
										<img src="../assets/img/e_info.png">
									</div>
									<div class="e_feed_inner floatLeft">
										<h6>Announcement Title:</h6>
										<h4>Eoffice Attendance Marking - Mandatory</h4>
									</div>
									<div class="clear"></div>
								</div>
								<div class="e_feed_content">
									<h6>Announcement Info:</h6>
									<div class="e_feeds_image">
										<ul>
											<li><img src="../assets/img/e_office1.png"></li>
											<li><img src="../assets/img/e_office2.png"></li>
											<div class="clear"></div>
										</ul>
									</div>
									<div class="e_feeds_messages">
										<p>Hi Team, </p>
										<p>It has been observed most employees were using “Custom Attendance”option in Eoffice for marking attendance instead of “Daily Attendance”. </p>
										<p>Custom Attendance markings can be used only when there are any Eoffice or network problems you are facing at the time of login. It must be done with proper explanation of reason with the approval of your respective Team Lead or appropriate person. </p>
										<p>Kindly take a note of the same.Looking forward for a positive outcome.</p>
										<p>Regards<br>
										HR</p>
									</div>
									<div class="e_feed_btm">
										<div class="e_feed_view floatLeft">View</div>
										<div class="e_feed_daten_time floatLeft">
											<span><img src="../assets/img/e_timeout.png"></span>
											<span>20&nbsp;/&nbsp;05&nbsp;/&nbsp;2017</span>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<span>10&nbsp;:&nbsp;00&nbsp;am</span>
										</div>
										<div class="clear"></div>
									</div>
									<br>	
									<hr>
									<br>
								</div>
							</li>					
						</ul>

	<!-- ENDS-FEEDS-LIST -->

					</div>
				</div>
			</div>

	<!-- ENDS-LEFT-PART-FEEDS -->

	<!-- RIGHT-PART-INFO -->

			<div class="col-md-4">
				<div class="e_content_wrapper">

	<!-- BIRTHDAY-INFO-HEADER -->

					<div class="e_content_head">
						<div class="e_title_circle color1 floatLeft">
							<img src="../assets/img/e_birthday.png">
						</div>
						<h5 class="floatLeft">Birthday Info</h5>
						<a href="view_birthday.php">
						<p class="floatRight">See all<span><img src="../assets/img/e_see_all.png"></span></p>
						</a>
						<div class="clear"></div>				
					</div>

	<!-- ENDS-BIRTHDAY-INFO-HEADER -->

					<div class="e_info_content">
						<div class="e_info_head">
							Upcoming Birthdays
						</div>

	<!-- BIRTHDAY-LIST -->

						<ul>
							<li>
								<div class="e_info_pic_div floatLeft">
									<img src="../assets/img/e_pic1.jpg">
								</div>
								<div class="e_info_detail_div floatLeft">
									<h5>Morgan Freeman</h5>
									<h6>17th June</h6>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="e_info_pic_div floatLeft">
									<img src="../assets/img/e_pic2.jpg">
								</div>
								<div class="e_info_detail_div floatLeft">
									<h5>Logan Vale</h5>
									<h6>28th June</h6>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="e_info_pic_div floatLeft">
									<img src="../assets/img/e_pic4.jpg">
								</div>
								<div class="e_info_detail_div floatLeft">
									<h5>Christina Lorenz</h5>
									<h6>10th July</h6>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="e_info_pic_div floatLeft">
									<img src="../assets/img/e_pic5.jpg">
								</div>
								<div class="e_info_detail_div floatLeft">
									<h5>Heather Marks</h5>
									<h6>15th July</h6>
								</div>
								<div class="clear"></div>
							</li>
						</ul>

	<!-- ENDS-BIRTHDAY-LIST -->

					</div>

	<!-- EVENT-INFO-HEADER -->

					<div class="e_content_head">
						<div class="e_title_circle color1 floatLeft">
							<img src="../assets/img/e_event.png">
						</div>
						<h5 class="floatLeft">Event Info</h5>
						<a href="view_events.php">
						<p class="floatRight">See all<span><img src="../assets/img/e_see_all.png"></span></p>
						</a>
						<div class="clear"></div>				
					</div>

	<!-- ENDS-EVENT-INFO-HEADER -->

	<!-- EVENT-LIST -->	

					<div class="e_event_list">
						<ul>
							<li>
								<div class="e_calender_event floatLeft">
									<img src="../assets/img/e_calender.png">
								</div>
								<div class="e_event_detail floatLeft">
									<h5>Halloween</h5>
									<h6>27th Oct 2017</h6>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="e_calender_event floatLeft">
									<img src="../assets/img/e_calender.png">
								</div>
								<div class="e_event_detail floatLeft">
									<h5>Christmas</h5>
									<h6>25th Dec 2017</h6>
								</div>
								<div class="clear"></div>
							</li>
						</ul>
					</div>

	<!-- ENDS-EVENT-LIST -->			




				</div>
			</div>

	<!-- ENDS-RIGHT-PART-INFO -->

		</div>

	<!-- ENDS-PARTITION -->

		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


