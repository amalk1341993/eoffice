
<?php
	include "../includes/header.php";
?>
<style>
	.e_main_navbar{display:none;}
	.e_main_footer{display:none;}
</style>

	<main role="main" class="widthFull heightFull">

	<!-- MAIN-BODY -->
	
	<div class="e_landing_main_div widthFull heightFull theme_secondary">
	
	<!-- LOGIN-FORM -->
	
	<div class="e_login_container">
	
	<!-- LOGO-DIV -->
		
		<div class="e_logo_div textCenter">
			<img src="../assets/img/e_logo.png">
			<p><span>20&nbsp;/&nbsp;05&nbsp;/&nbsp;2017</span><span class="e_logo_bar">|</span><span>10&nbsp;:&nbsp;00&nbsp;am</span></p>
		</div>
	
	<!-- END-LOGO-DIV -->
	
	<!-- LOGIN-WRAP -->
	
	<div class="e_login_wrap">
		<h4>Welcome back</h4>
		<p>to Techware <a>e-office</a></p>
		<hr>
		<div class="e_login_form_row">
			<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
				<input class="mdl-textfield__input e_login_input" type="text" id="sample1">
				<label class="mdl-textfield__label e_login_mdl_label" for="uname">Username</label>
			</div>
		</div>
		<div class="e_login_form_row">
			<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
				<input class="mdl-textfield__input e_login_input" type="text" id="sample1">
				<label class="mdl-textfield__label e_login_mdl_label" for="password">Password</label>
			</div>
		</div>
		<div class="e_login_form_row textCenter">
			<a href="home_index.php">
			<button class="e_signin_btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect e_signin_btn  btn-sm">Signin<i class="fa fa-angle-right" aria-hidden="true"></i></button>
			</a>
		</div>
		<div class="e_login_form_row p0 textCenter">
			<p>Forgot Password?</p>
		</div>
	</div>
	
	<!-- END-LOGIN-WRAP -->
	
	</div>
	
	<!-- END-LOGIN-FORM -->
	
	<!-- LANDING-FOOTER -->
	
		<div class="e_landing_footer textCenter">
			<p class="font_size_xs">Copyright © techware.co.in</p>
			<div class="e_landing_bottom_bar"></div>
		</div>
		
	<!-- END-LANDING-FOOTER -->
	
	</div>

	<!-- END-MAIN-BODY -->
	
	</main>
	
<?php
	include "../includes/footer.php";
?>

	

