<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull heightFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Leave Status
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

					</div>
				</div>
			</div>

	<!-- ENDS-PARTITION -->

	<!-- LEAVE-STATUS-TABLES -->

			<div class="e_leave_status_table">

	<!-- LEAVE-STATUS-HEAD -->

				<div class="e_leave_status_head">
					<div class="row">
						<div class="col-md-1">
							<select class="e_leave_status_sl">
								<option>10</option>
								<option>20</option>
							</select>
						</div>
						<div class="col-md-2"><span>Sort by:</span>
							<select class="e_leave_status_sort">
								<option>Date</option>
								<option>Time</option>
							</select>
						</div>
						<div class="col-md-5"></div>
						<div class="col-md-4">
							<div class="e_leave_search_box">
								<input class="e_leave_search_input" placeholder="Search here">
							</div>
						</div>
					</div>
				</div>

	<!-- ENDS-LEAVE-STATUS-HEAD -->

	<!-- LEAVE-STATUS-TABLE-DIV -->

				<div class="table-responsive">          
				  	<table class="table">
				    	<thead>
				      		<tr>
						        <th>Username</th>
						        <th>From date</th>
						        <th>Day Mention (Half/Full)</th>
						        <th>Leave Reason</th>
						        <th>Apply Date</th>
						        <th>Status</th>
						        <th>Cancel</th>
				      		</tr>
				    	</thead>
					    <tbody>
					      <tr>
					        <td>Sophie Lewis</td>
					        <td>27-10-2017</td>
					        <td>Full Day</td>
					        <td>Medical Leave</td>
					        <td>25-10-2017</td>
					        <td class="pending">Pending</td>
					        <td class="cancel"></td>
					      </tr>
					      <tr>
					        <td>Sophie Lewis</td>
					        <td>27-10-2017</td>
					        <td>Full Day</td>
					        <td>Medical Leave</td>
					        <td>25-10-2017</td>
					        <td class="approved">Approved</td>
					        <td class="cancel"></td>
					      </tr>
					      <tr>
					        <td>Sophie Lewis</td>
					        <td>27-10-2017</td>
					        <td>Full Day</td>
					        <td>Medical Leave</td>
					        <td>25-10-2017</td>
					        <td class="pending">Pending</td>
					        <td class="cancel"></td>
					      </tr>
					    </tbody>
				  	</table>
 				</div>


 	<!-- ENDS-LEAVE-STATUS-TABLE-DIV -->

			</div>

	<!-- ENDS-LEAVE-STATUS-TABLES -->

		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


