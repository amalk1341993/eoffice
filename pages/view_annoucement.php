<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Announcement
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

					</div>
				</div>
			</div>
			<hr>

	<!-- THOUGHT-OF-THE-DAY-DIV -->

			<div class="e_thought_div">

	<!-- THOUGHT-DIV-LEFT-->

				<div class="e_thought_div_left">
					<img src="../assets/img/e_announcement.png">
				</div>

	<!-- ENDS-THOUGHT-DIV-LEFT -->

	<!-- THOUGHT-DIV-RIGHT-->

				<div class="e_annoucement_list_div_right">
					<div class="row">
						<div class="col-md-8">

	<!-- ANNOUNCEMENT-DIV -->

							<ul>
								<li>
									<div class="e_feed_title">
										<div class="e_feed_inner floatLeft p0">
											<h4>Eoffice Attendance Marking - Mandatory</h4>
										</div>
										<div class="clear"></div>
										<div class="e_feed_btm">
											<div class="e_feed_daten_time floatLeft m0">
													<span><img src="../assets/img/e_timeout.png"></span>
													<span>20&nbsp;/&nbsp;05&nbsp;/&nbsp;2017</span>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<span>10&nbsp;:&nbsp;00&nbsp;am</span>
											</div>
											<div class="clear"></div>
										</div>
										<br>
									</div>
									<div class="e_feed_content p0">
										<h6>Announcement Info:</h6>
												<div class="e_feeds_image">
													<ul>
														<li><img src="../assets/img/e_office1.png"></li>
														<li><img src="../assets/img/e_office2.png"></li>
														<div class="clear"></div>
													</ul>
												</div>
										<div class="e_feeds_messages">
											<p>Hi Team, </p>
											<p>It has been observed most employees were using “Custom Attendance”option in Eoffice for marking attendance instead of “Daily Attendance”. </p>
											<p>Custom Attendance markings can be used only when there are any Eoffice or network problems you are facing at the time of login. It must be done with proper explanation of reason with the approval of your respective Team Lead or appropriate person. </p>
											<p>Kindly take a note of the same.Looking forward for a positive outcome.</p>
											<p>Regards<br>
											HR</p>
										</div>
									</div>
									<hr>
								</li>
								<li>
									<div class="e_feed_title">
										<div class="e_feed_inner floatLeft p0">
											<h4>Eoffice Attendance Marking - Mandatory</h4>
										</div>
										<div class="clear"></div>
										<div class="e_feed_btm">
											<div class="e_feed_daten_time floatLeft m0">
													<span><img src="../assets/img/e_timeout.png"></span>
													<span>20&nbsp;/&nbsp;05&nbsp;/&nbsp;2017</span>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<span>10&nbsp;:&nbsp;00&nbsp;am</span>
											</div>
											<div class="clear"></div>
										</div>
										<br>
									</div>
									<div class="e_feed_content p0">
										<h6>Announcement Info:</h6>
												<div class="e_feeds_image">
													<ul>
														<li><img src="../assets/img/e_office1.png"></li>
														<li><img src="../assets/img/e_office2.png"></li>
														<div class="clear"></div>
													</ul>
												</div>
										<div class="e_feeds_messages">
											<p>Hi Team, </p>
											<p>It has been observed most employees were using “Custom Attendance”option in Eoffice for marking attendance instead of “Daily Attendance”. </p>
											<p>Custom Attendance markings can be used only when there are any Eoffice or network problems you are facing at the time of login. It must be done with proper explanation of reason with the approval of your respective Team Lead or appropriate person. </p>
											<p>Kindly take a note of the same.Looking forward for a positive outcome.</p>
											<p>Regards<br>
											HR</p>
										</div>
									</div>
									<hr>
								</li>
								<li>
									<div class="e_feed_title">
										<div class="e_feed_inner floatLeft p0">
											<h4>Eoffice Attendance Marking - Mandatory</h4>
										</div>
										<div class="clear"></div>
										<div class="e_feed_btm">
											<div class="e_feed_daten_time floatLeft m0">
													<span><img src="../assets/img/e_timeout.png"></span>
													<span>20&nbsp;/&nbsp;05&nbsp;/&nbsp;2017</span>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<span>10&nbsp;:&nbsp;00&nbsp;am</span>
											</div>
											<div class="clear"></div>
										</div>
										<br>
									</div>
									<div class="e_feed_content p0">
										<h6>Announcement Info:</h6>
												<div class="e_feeds_image">
													<ul>
														<li><img src="../assets/img/e_office1.png"></li>
														<li><img src="../assets/img/e_office2.png"></li>
														<div class="clear"></div>
													</ul>
												</div>
										<div class="e_feeds_messages">
											<p>Hi Team, </p>
											<p>It has been observed most employees were using “Custom Attendance”option in Eoffice for marking attendance instead of “Daily Attendance”. </p>
											<p>Custom Attendance markings can be used only when there are any Eoffice or network problems you are facing at the time of login. It must be done with proper explanation of reason with the approval of your respective Team Lead or appropriate person. </p>
											<p>Kindly take a note of the same.Looking forward for a positive outcome.</p>
											<p>Regards<br>
											HR</p>
										</div>
									</div>
									<hr>
								</li>
							</ul>

	<!-- ENDS-ANNOUCEMENT-DIV -->
							
						</div>
						<div class="col-md-4"></div>
					</div>			
				</div>

	<!-- ENDS-THOUGHT-DIV-RIGHT -->

				<div class="clear"></div>

			</div>
	
	<!-- END-THOUGHT-OF-THE-DAY-DIV -->		

	<!-- ENDS-PARTITION -->



		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


