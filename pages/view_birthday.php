<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull heightFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Birthday
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

					</div>
				</div>
			</div>
			<hr>

	<!-- THOUGHT-OF-THE-DAY-DIV -->

			<div class="e_thought_div">

	<!-- THOUGHT-DIV-LEFT-->

				<div class="e_thought_div_left">
					<img src="../assets/img/e_birthday1.png">
				</div>

	<!-- ENDS-THOUGHT-DIV-LEFT -->

	<!-- THOUGHT-DIV-RIGHT-->

				<div class="e_birthday_list_div_right">
					<ul>
						<li>
							<div class="e_expand"></div>
							<div class="e_birthday_profile_pic">
								<img src="../assets/img/e_pic3.jpg">
							</div>
							<h5>TONY STARK</h5>
							<ul>
								<li>
									<div class="child">Department</div>
									<div class="child">: &nbsp;&nbsp;Developement</div>
									<div class="clear"></div>
								</li>
								<li>
									<div class="child">Designation</div>
									<div class="child">: &nbsp;&nbsp;UI/UX Developer</div>
									<div class="clear"></div>
								</li>
								<li>
									<div class="child">Employee ID</div>
									<div class="child">: &nbsp;&nbsp;TWS 125</div>
									<div class="clear"></div>
								</li>
								<li>
									<div class="child">DOB</div>
									<div class="child">: &nbsp;&nbsp;13 / 10 / 1993</div>
									<div class="clear"></div>
								</li>
							</ul>
						</li>
						<li>
							<div class="e_expand"></div>
							<div class="e_birthday_profile_pic">
								<img src="../assets/img/e_pic6.jpg">
							</div>
							<h5>MIA ROBERTSON</h5>
							<ul>
								<li>
									<div class="child">Department</div>
									<div class="child">: &nbsp;&nbsp;Developement</div>
									<div class="clear"></div>
								</li>
								<li>
									<div class="child">Designation</div>
									<div class="child">: &nbsp;&nbsp;UI/UX Developer</div>
									<div class="clear"></div>
								</li>
								<li>
									<div class="child">Employee ID</div>
									<div class="child">: &nbsp;&nbsp;TWS 125</div>
									<div class="clear"></div>
								</li>
								<li>
									<div class="child">DOB</div>
									<div class="child">: &nbsp;&nbsp;13 / 10 / 1993</div>
									<div class="clear"></div>
								</li>
							</ul>
						</li>
					</ul>
				</div>

	<!-- ENDS-THOUGHT-DIV-RIGHT -->

				<div class="clear"></div>

			</div>
	
	<!-- END-THOUGHT-OF-THE-DAY-DIV -->		

	<!-- ENDS-PARTITION -->



		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


