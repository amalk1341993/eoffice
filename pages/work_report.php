<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				My Work Report
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">
				<div class="col-md-12">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<div class="row">
								<div class="col-md-2">
									<p>Current Time</p>
									<h3><span id="theTime"></span></h3>
								</div>
								<div class="col-md-10">
									
								</div>
							</div>
						</div>

	<!-- ENDS-CURRENT-TIME -->

					</div>
				</div>
			</div>
			<hr>

	<!-- ATTENDENCE-STATUS -->

			<div class="e_attendence_status">
				<div class="row">
					<div class="col-md-12">
						<p>Select month and year to generate report</p>
						<div class="row">
							<div class="col-md-3"><h6>Select Month</h6></div>
							<div class="col-md-3"><h6>Select Year</h6></div>
							<div class="col-md-4"><h6>Select Report</h6></div>
							<div class="col-md-2"></div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<select class="e_attendence_select">
									<option>Month</option>
								</select>
							</div>
							<div class="col-md-3">
								<select class="e_attendence_select">
									<option>Year</option>
								</select>	
							</div>
							<div class="col-md-4">
								<div class="e_radio_button_div">
									<label class="e_radio_button inline"> 
									    <input type="radio" name="report" value="summary" checked>
									    <span>Summary Report</span> 
									</label>
									<label class="e_radio_button inline"> 
									    <input type="radio" name="report" value="detailed">
									    <span>Detailed Report</span> 
  									</label>
								</div>
							</div>
							<div class="col-md-2 textRight">
								<button class="e_attendence_submit mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">Submit</button>
							</div>
						</div>
						<br>
						<p>Users Attendance Report Info</p>
						<div class="row">
							<div class="col-md-6">
								<div class="e_attendence_report_info">
									<div class="e_attendence_report_pic">
										<img src="../assets/img/e_pic3.jpg">
									</div>
									<div class="e_attendence_report_detail">
										<ul>
											<li>
												<div class="child1">Username</div>
												<div class="child2">:&nbsp;&nbsp;&nbsp;&nbsp;James Walton</div>
												<div class="clear"></div>
											</li>
											<li>
												<div class="child1">Employee ID</div>
												<div class="child2">:&nbsp;&nbsp;&nbsp;&nbsp;TWS 123</div>
												<div class="clear"></div>
											</li>
											<li>
												<div class="child1">Report month/Year</div>
												<div class="child2">:&nbsp;&nbsp;&nbsp;&nbsp;May 2017</div>
												<div class="clear"></div>
											</li>
										</ul>
									</div>
									<div class="clear"></div>
								</div>
							</div>
							<div class="col-md-6"></div>
						</div>
					</div>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-md-12">

	<!-- LEAVE-STATUS-TABLES -->

						<div class="e_leave_status_table">

	<!-- LEAVE-STATUS-HEAD -->

							<div class="e_leave_status_head">
								<div class="row">
									<div class="col-md-1">
										<select class="e_leave_status_sl">
											<option>10</option>
											<option>20</option>
										</select>
									</div>
									<div class="col-md-2"><span>Sort by:</span>
										<select class="e_leave_status_sort">
											<option>Date</option>
											<option>Time</option>
										</select>
									</div>
									<div class="col-md-5"></div>
									<div class="col-md-4">
										<div class="e_leave_search_box">
											<input class="e_leave_search_input" placeholder="Search here">
										</div>
									</div>
								</div>
							</div>

	<!-- ENDS-LEAVE-STATUS-HEAD -->

	<!-- LEAVE-STATUS-TABLE-DIV -->

							<div class="table-responsive">          
							  	<table class="table">
							    	<thead>
							      		<tr>
									        <th>Report Date</th>
									        <th>Signin Time</th>
									        <th>Signout Time</th>
									        <th>Working Hours</th>
									        <th>Punchin Note</th>
									        <th>Punchout Note</th>
									        <th>Status</th>
									        <th>Report info</th>
							      		</tr>
							    	</thead>
								    <tbody>
								      <tr>
								        <td>05-may-2017</td>
								        <td>08:40 am</td>
								        <td>06:00 pm</td>
								        <td>09 hrs 30 mins</td>
								        <td>sfsffsf</td>
								        <td></td>
								        <td><div class="e_completed">Complete<i class="fa fa-check" aria-hidden="true"></i></div></td>
								        <td><div class="e_info" data-toggle="collapse" data-target="#demo">Info<i class="fa fa-info-circle" aria-hidden="true"></i></div>
								        </td>
								      </tr>
								      <tr>
								        <td>05-may-2017</td>
								        <td>08:40 am</td>
								        <td>06:00 pm</td>
								        <td>09 hrs 30 mins</td>
								        <td></td>
								        <td></td>
								        <td><div class="e_holiday">Holiday<i class="fa fa-circle-o" aria-hidden="true"></i></div></td>
								        <td><div class="e_info">Info<i class="fa fa-info-circle" aria-hidden="true"></i></div></td>
								      </tr>
								      <tr>
								        <td>05-may-2017</td>
								        <td>08:40 am</td>
								        <td>06:00 pm</td>
								        <td>09 hrs 30 mins</td>
								        <td></td>
								        <td>sfsffsfsf</td>
								        <td><div class="e_leave">Leave<i class="fa fa-info" aria-hidden="true"></i></div></td>
								        <td><div class="e_info">Info<i class="fa fa-info-circle" aria-hidden="true"></i></div></td>
								      </tr>
								    </tbody>
							  	</table>
			 				</div>


	<!-- ENDS-LEAVE-STATUS-TABLE-DIV -->

						</div>

					</div>
				</div>
			</div>

	<!-- ENDS-ATTENDENCE-STATUS -->
	
	<!-- ENDS-PARTITION -->



		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


