<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Custom Attendence
			</div>

	<!-- PARTITION-STARTS -->

			<div class="row">

	<!-- LEFT-PART-FEEDS -->

				<div class="col-md-8">
					<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

						<div class="e_current_time">
							<p>Current Time</p>
							<h3><span id="theTime"></span></h3>

						</div>

	<!-- ENDS-CURRENT-TIME -->

	<!-- ATTENDENCE-FOR-THE-DAY-DIV -->

						<div class="e_attendence_for_day">
							<p>Attendence for the day<strong>02 May 2017 </strong></p>
						</div>

	<!-- ENDS-ATTENDENCE-FOR-THE-DAY-DIV -->

	<!-- ATTENDENCE-FROM-TO-DIV -->

						<div class="e_attendence_from_to_div">
							<div class="row m0">
								<div class="col-md-5 col-xs-5 p0">
									<div class="e_attendence_from">
										<h4>Working Hours</h4>
										<p>08 : 00 <strong>hrs</strong></p>
									</div>
								</div>
								<div class="col-md-7 col-xs-7 p0">
									<div class="e_attendence_to">
										<h4>Last Punchin Time</h4>
										<p>08 : 30 am</p>
									</div>
								</div>
							</div>
						</div>

	<!-- ENDS-ATTENDENCE-FROM-TO-DIV -->

	<!-- REMARKS-DIV -->

						<div class="e_attendence_remarks">
							<textarea class="e_attendence_textarea" rows="4" placeholder="Enter your remarks"></textarea>
						</div>

	<!-- ENDS-REMARKS-DIV -->

	<!-- MARK-BUTTONS -->

						<div class="e_marks_btn_bay">
							<p>Time Picker</p>
							<div class="e_time_picker_div">
								<select class="e_time_picker">
									<option>01</option>						
								</select>
								<span>:</span>
								<select class="e_time_picker">
									<option>01</option>									
								</select>
							</div>
							<div class="clear"></div>
							<button class="e_mark_in floatLeft mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">Custom Mark in <i class="fa fa-sign-in" aria-hidden="true"></i></button>
							<button class="e_mark_custom_out floatRight mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">Custom Mark Out<i class="fa fa-sign-out" aria-hidden="true"></i></button>
							<div class="clear"></div>
						</div>

	<!-- ENDS-MARKS-BUTTONS -->

					</div>
				</div>

	<!-- ENDS-LEFT-PART-FEEDS -->

	<!-- RIGHT-PART-INFO -->

				<div class="col-md-4">
					<div class="e_content_wrapper">

	<!-- EVENT-INFO-HEADER -->

						<div class="e_content_head">
							<div class="e_title_circle color1 floatLeft">
								<img src="../assets/img/e_info.png">
							</div>
							<h5 class="floatLeft">Announcement</h5>
							<p class="floatRight">See all<span><img src="../assets/img/e_see_all.png"></span></p>
							<div class="clear"></div>				
						</div>

	<!-- ENDS-EVENT-INFO-HEADER -->

	<!-- ANNOUNCEMENT-DIV -->

						<div class="e_announcement_div">
							<div class="e_feed_title">
								<div class="e_feed_btm">
									<div class="e_feed_daten_time floatLeft m0">
											<span><img src="../assets/img/e_timeout.png"></span>
											<span>20&nbsp;/&nbsp;05&nbsp;/&nbsp;2017</span>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<span>10&nbsp;:&nbsp;00&nbsp;am</span>
									</div>
								</div>
								<div class="e_feed_inner floatLeft p0">
									<h4>Eoffice Attendance Marking - Mandatory</h4>
								</div>
								<div class="clear"></div>
							</div>
							<div class="e_feed_content p0">
								<div class="e_feeds_messages">
									<p>Hi Team, </p>
									<p>It has been observed most employees were using “Custom Attendance”option in Eoffice for marking attendance instead of “Daily Attendance”. </p>
									<p>Custom Attendance markings can be used only when there are any Eoffice or network problems you are facing at the time of login. It must be done with proper explanation of reason with the approval of your respective Team Lead or appropriate person. </p>
									<p>Kindly take a note of the same.Looking forward for a positive outcome.</p>
									<p>Regards<br>
									HR</p>
								</div>
								<div class="e_feed_btm">
									<div class="e_feed_view floatLeft">View</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

	<!-- ENDS-ANNOUCEMENT-DIV -->

					</div>
				</div>

	<!-- ENDS-RIGHT-PART-INFO -->

			</div>

	<!-- ENDS-PARTITION -->

	<!-- TIME-ZONE-DIV -->

			<div class="e_time_zones">
			<hr>
				<ul>
					<li>
						<h6>US</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag1.png">
						</div>
						<p>02:00 am</p>
					</li>
					<li>
						<h6>Britain</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag2.png">
						</div>
						<p>02:00 am</p>
					</li>
					<li>
						<h6>Holand</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag3.png">
						</div>
						<p>02:00 am</p>
					</li>
					<li>
						<h6>US</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag4.png">
						</div>
						<p>02:00 am</p>
					</li>
					<li>
						<h6>US</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag5.png">
						</div>
						<p>02:00 am</p>
					</li>
					<li>
						<h6>US</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag6.png">
						</div>
						<p>02:00 am</p>
					</li>
					<li>
						<h6>US</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag7.png">
						</div>
						<p>02:00 am</p>					
					</li>
					<li>
						<h6>US</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag8.png">
						</div>
						<p>02:00 am</p>
					</li>
					<li>
						<h6>US</h6>
						<div class="e_flag">
							<img src="../assets/img/e_flag9.png">
						</div>
						<p>02:00 am</p>
					</li>
				</ul>
			</div>

	<!-- ENDS-TIME-ZONE-DIV -->	

		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


