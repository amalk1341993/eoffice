<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull heightFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Temporary work status
			</div>

	<!-- PARTITION-STARTS -->

		<div class="row">
			<div class="col-md-12">
				<div class="e_content_wrapper">

	<!-- CURRENT-TIME-DIV -->

					<div class="e_current_time">
						<div class="row">
							<div class="col-md-2">
								<p>Current Time</p>
								<h3><span id="theTime"></span></h3>
							</div>
							<div class="col-md-10">
								<div class="e_status_for_day">
									<p>Status for the day<strong>02 May 2017</strong></p>
								</div>
							</div>
						</div>
					</div>

	<!-- ENDS-CURRENT-TIME -->

	<!-- WORK-STATUS-DIV -->

					<div class="e_work_status_div">

	<!-- WORK-STATUS-HEAD -->

						<div class="e_work_status_head">
							<div class="row">
								<div class="col-md-3">
									<div class="e_work_status_circle">
										<img src="../assets/img/e_p1.png">
									</div>
									<p>Project name</p>
									<div class="clear"></div>
								</div>
								<div class="col-md-2">
									<div class="e_work_status_circle">
										<img src="../assets/img/e_p2.png">
									</div>
									<p>Work Type</p>
									<div class="clear"></div>							
								</div>
								<div class="col-md-2">
									<div class="e_work_status_circle">
										<img src="../assets/img/e_p3.png">
									</div>
									<p>No. of Hour</p>
									<div class="clear"></div>
								</div>
								<div class="col-md-5">
									<div class="e_work_status_circle">
										<img src="../assets/img/e_p4.png">
									</div>
									<p>Comments</p>
									<div class="clear"></div>
								</div>
							</div>
						</div>

	<!-- ENDS-WORK-STATUS-HEAD -->

	<!-- WORK-STATUS-CONTENT -->

						<div class="e_work_status_content">
							<ul>
								<li>
									<div class="row">
										<div class="col-md-3">
											<select class="e_work_status_select">
												<option>Select Project name</option>
											</select>
										</div>
										<div class="col-md-2">
											<select class="e_work_status_select">
												<option>Work Type</option>
											</select>							
										</div>
										<div class="col-md-2">

											<select class="e_work_status_select1 floatLeft">
												<option>00</option>
											</select>
											<span>:</span>
											<select class="e_work_status_select1 floatRight">
												<option>00</option>
											</select>
											<div class="clear"></div>
										</div>
										<div class="col-md-5">
											<textarea class="e_work_status_textarea floatLeft" rows="3" placeholder="Comments here">
											</textarea>
											<button class="e_work_add_btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect floatRight">Add<i class="fa fa-plus" aria-hidden="true"></i></button>
										</div>
									</div>
								</li>
							</ul>
						</div>

	<!-- ENDS-WORK-STATUS-CONTENT -->

					</div>

	<!-- ENDS-WORK-STATUS -->

				</div>
			</div>
		</div>

	<!-- ENDS-PARTITION -->

		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


