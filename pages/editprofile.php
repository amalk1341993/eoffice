<?php
	include "../includes/header.php";
?>
    <main role="main" class="heightFull">
	
	<!-- MAIN-BODY -->
	
	<div class="e_main_div widthFull  theme_primary">

	<!-- CONTAINER-STARTS -->

		<div class="container custom_container">
			<div class="e_landing_page_title">
				Edit Profile
			</div>

	<!-- EDIT-PROFILE-BANNER-STARTS -->

			<div class="e_edit_profile_banner">
				<div class="e_edit_profile_pic floatLeft">
					<div class="e_profile_detail_pic_overlay"></div>
					<img src="../assets/img/e_pic4.jpg">
				</div>
				<div class="e_edit_profile_detail floatLeft">
					<h3>Ann Alexander</h3>
					<h5>Software Developer</h5>
					<p>EMP ID: TWS 134</p>
				</div>
				<div class="clear"></div>
			</div>

	<!-- ENDS-EDIT-PROFILE-BANNER-STARTS -->

	<!-- EDIT-PROFILE-DETAIL-STARTS -->

			<div class="e_edit_profile_detail_div">
				<div class="row">
					<div class="col-md-5">
						<div class="e_edit_profile_section">
							<div class="e_edit_profile_row">
								<p>Name</p>
								<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
									<input class="mdl-textfield__input e_edit_input" type="text" id="sample1">
									<label class="mdl-textfield__label e_edit_input_label" for="uname">Ann Alexander</label>
								</div>
							</div>
							<div class="e_edit_profile_row">
								<p>Gender</p>
								<div class="e_radio_button_div">
									<label class="e_radio_button inline"> 
									    <input type="radio" name="report" value="summary" checked>
									    <span>Male</span> 
									</label>
									<label class="e_radio_button inline"> 
									    <input type="radio" name="report" value="detailed">
									    <span>Female</span> 
  									</label>
								</div>
							</div>
							<div class="e_edit_profile_row">
								<p>Email</p>
								<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
									<input class="mdl-textfield__input e_edit_input" type="text" id="sample1">
									<label class="mdl-textfield__label e_edit_input_label" for="uname">annalexa@gmail.com</label>
								</div>
							</div>
							<div class="e_edit_profile_row">
								<p>Address</p>
								<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
									<textarea class="mdl-textfield__input e_edit_input" type="text" id="sample1" rows="4" style="height:auto;"></textarea>
									<label class="mdl-textfield__label e_edit_input_label" for="uname">Dummy Address</label>
								</div>
							</div>
							<div class="e_edit_profile_row">
								<p>Date of Birth</p>
								<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
									<input class="mdl-textfield__input e_edit_input" type="text" id="sample1">
									<label class="mdl-textfield__label e_edit_input_label" for="uname">13 - 04 - 1993</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="e_edit_profile_section borderNone">
							<div class="e_edit_profile_row">
								<p>Qualification</p>
								<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
									<input class="mdl-textfield__input e_edit_input" type="text" id="sample1">
									<label class="mdl-textfield__label e_edit_input_label" for="uname">B tech ComputerScience Engineering</label>
								</div>
							</div>
							<div class="e_edit_profile_row">
								<p>Phone number</p>
								<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
									<input class="mdl-textfield__input e_edit_input" type="text" id="sample1">
									<label class="mdl-textfield__label e_edit_input_label" for="uname">+91 9961531199</label>
								</div>
							</div>
							<div class="e_edit_profile_row">
								<p>Blood Group</p>
								<div class="mdl-textfield mdl-js-textfield e_login_mdl_textfield ">
									<input class="mdl-textfield__input e_edit_input" type="text" id="sample1">
									<label class="mdl-textfield__label e_edit_input_label" for="uname">A+</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

	<!-- ENDS-EDIT-PROFILE-DETAIL -->

	<!-- EDIT-PROFILE-BUTTON-BAY -->

			<div class="e_edit_profile_btn_bay">
				<button class="e_save_changes_btn">Save changes<img src="../assets/img/e_check.png"></button>
				<button class="e_reset_btn">Reset</button>
			</div>


	<!-- ENDS-EDIT-PROFILE-BUTTON-BAY -->

		</div>

	<!-- END-CONTAINER -->

	</div>

	<!-- END-MAIN-BODY -->
	 
    </main>
	
	
<?php
	include "../includes/footer.php";
?>


