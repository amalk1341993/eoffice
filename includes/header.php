
	<!-- HEADER-START -->

<!DOCTYPE html>

<html lang="en">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../assets/img/e_logo_nav_bar.png">

	<!-- WEB-TITLE -->

		<title>E Office Portal</title>

    <!-- INCLUDED-CASCADE-STYLE-SHEETS -->

		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/custom.css" rel="stylesheet">
		<link href="../assets/css/theme.css" rel="stylesheet">
		<link href="../assets/css/responsive.css" rel="stylesheet">
		<link href="../assets/css/material.min.css" rel="stylesheet">
		<link href="../assets/css/datepicker3.css" rel="stylesheet">
		<link href="../assets/css/font-awesome.min.css" rel="stylesheet">
	</head>

	<body class="widthFull heightFull">

	<!-- NAVIGATION-BAR -->

    <nav class="navbar navbar-expand-md fixed-top e_main_navbar">

    <!-- NAVIGATION-CONTAINER -->

		<div class="container custom_container">
			<a class="navbar-brand" href="home_index.php"><img class="e_logo_expand" src="../assets/img/e_logo_nav_bar.png"><img class="e_logo_shrink" src="../assets/img/e_logo_shrink.png"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<div class="container">
					<div class="e_profile_account floatRight" id="menu7">
						<div class="e_nav_profile_pic floatRight">
							<span><img src="../assets/img/e_drop_down_orange.png"></span>
						</div>
						<div class="e_nav_profile floatRight">
							<p>Sophia Lewis</p>
						</div>
						<ul class="e_account_drop mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu7">
							<a href="myprofile.php">
								<li><i class="fa fa-user" aria-hidden="true"></i>My Profile</li>
							</a>
							<li><i class="fa fa-key" aria-hidden="true"></i>Change Password</li>
							<a href="editprofile.php">
								<li class="mdl-menu__item--full-bleed-divider"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Profile</li>
							</a>
							<li><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</li>
						</ul>
					</div>


	<!-- NAVIGATION-MENU-LIST -->

					<div  class="e_nav_bar_menu_list">
						<ul>
							<li><a class="text_color_white">Welcome</a></li>
							<li id="menu1">Attendence<span><img src="../assets/img/e_drop_down.png"></span></li>
								<ul class="e_nav_menu_drop_down mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu1">
								  <li>
								  	<a href="attendence.php">
								  Daily Attendence
								  	</a>
								  </li>
								  <li>
								  	<a href="custom_attendence.php">
								  Custom Attendence
								  	</a>
								  </li>
								</ul>
							<li id="menu2">Work<span><img src="../assets/img/e_drop_down.png"></span></li>
								<ul class="e_nav_menu_drop_down mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu2">
								  <li>Work Status</li>
								  <li>
								  	<a href="temp_work_status.php">
								  Temporary Work Status
								  	</a>
								  </li>
								</ul>
							<li id="menu3">Leave<span><img src="../assets/img/e_drop_down.png"></span></li>
								<ul class="e_nav_menu_drop_down mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu3">
								  <li>
								  	<a href="apply_leave.php">
								  Apply Leave
								  	</a>
								  </li>
								  <li>
								  	<a href="leave_status.php">
								  Leave Status
									</a>
								</li>
								  <li>
								  	<a href="leave_summary.php">
								  Leave Summary
								  	</a>
								</li>
								</ul>
							<li id="menu4">View<span><img src="../assets/img/e_drop_down.png"></span></li>
								<ul class="e_nav_menu_drop_down mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu4">
								  <li>
								  	<a href="thought_of_day.php">
								  	Though of the Day
								  	</a>
								  </li>
								  <li>Appreciation</li>
								  <li>
								  	<a href="view_birthday.php">
								  Birthday
									</a>
									</li>
								  <li>
								  	<a href="view_annoucement.php">
								  Announcement
									</a>
								  </li>
								  <li>
								  	<a href="view_events.php">
								  Events
								  	</a>
								</li>
								</ul>
							<li id="menu5">Reports<span><img src="../assets/img/e_drop_down.png"></span></li>
								<ul class="e_nav_menu_drop_down mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu5">
								  <li>
								  	<a href="attendence_report.php">
								  My Attendence
									</a>
								  </li>
								  <li>
								  	<a href="work_report.php">
								  My Work Report
								  	</a>
									</li>
								</ul>
							<li id="menu6">Project<span><img src="../assets/img/e_drop_down.png"></span></li>
								<ul class="e_nav_menu_drop_down mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu6">
								  <li>
								  	<a href="myproject.php">
								  My Projects
									</a>
								</li>
	
								</ul>
							<div class="clear"></div>
						</ul>
					</div>

	<!-- NAVIGATION-MENU-LIST-ENDS -->	
				
					<div class="clear"></div>
				</div>
			</div>
		</div>

	<!-- NAVIGATION-CONTAINER-ENDS -->

    </nav>

	<!-- END-NAVIGATION-BAR -->

	<!-- END-HEADER -->

	<!-- CHANGE-PASSWORD-MODAL-STARTS-->

	<div id="changepass" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
	    	<div class="modal-content e_change_password_modal_content">
	      		<div class="e_change_password_modal_container">
	        		<div class="e_content_head">
						<div class="e_title_circle color1 floatLeft">
							<img src="../assets/img/e_lock.png">
						</div>
						<h5 class="floatLeft">Change Password</h5>
						<div class="clear"></div>				
					</div>
					<div class="e_change_password_content">
						<div class="e_change_pass_row">
							<p>Old Password</p>
							<input class="e_change_pass_input" placeholder="Old Password">
						</div>
						<div class="e_change_pass_row">
							<p>New Password</p>
							<input class="e_change_pass_input" placeholder="New Password">
						</div>
						<div class="e_change_pass_row">
							<p>Confirm New Password</p>
							<input class="e_change_pass_input" placeholder="Confirm New Password">
						</div>
						<div class="e_change_pass_row">
							<div class="row">
								<div class="col-md-6"><button class="e_change_pass_submit" data-dismiss="modal">Submit</button></div>
								<div class="col-md-6"><button class="e_change_pass_reset" data-dismiss="modal">Reset</button></div>
							</div>
						</div>
					</div>
	      		</div>
	   		 </div>
	  	</div>
	</div>

    <!-- ENDS-CHANGE-PASSWORD-MODAL-->
